#!/bin/bash
#SBATCH -p regular
#SBATCH -t 03:30:00
#SBATCH -C haswell
#SBATCH -J my_job
#SBATCH -o my_job.o%j

EXE=/global/cscratch1/sd/preeti/work/mpieg/rw-benchmark/fileWr

echo ${SLURM_JOB_NODELIST}
ranks=$((32*${SLURM_JOB_NUM_NODES}))

echo `date`
for iter in 1 2 3
do
for size in 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 8388608 16777216 33554432 #67108864 134217728 #268435456 536870912 
do
  srun -n $ranks -N ${SLURM_JOB_NUM_NODES} $EXE $size 
  #xtnodestat
  srun -n $ranks -N ${SLURM_JOB_NUM_NODES} $EXE $size 
  #xtnodestat
  srun -n $ranks -N ${SLURM_JOB_NUM_NODES} $EXE $size 
  #xtnodestat
done
done

echo `date`
